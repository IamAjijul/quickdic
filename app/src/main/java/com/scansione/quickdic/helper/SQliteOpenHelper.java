package com.scansione.quickdic.helper;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.scansione.quickdic.tables.DicTable;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 * Created by cn74 on 3/7/17.
 */

public class SQliteOpenHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "QuickDic";
    private static final int DATABASE_VERSION = 1;
    private final String TABLE_NAME = "Dictionary";
    private final String WORD = "WORD";
    private final String CLASS = "CLASS";
    private final String MEANING = "MEANING";


    public SQliteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_DICTIONARY_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + WORD + " TEXT," + CLASS + " TEXT,"
                + MEANING + " TEXT" + ")";
        db.execSQL(CREATE_DICTIONARY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public void insertDataIntoDictionary(Elements elements) {
        SQLiteDatabase database = this.getWritableDatabase();

        DatabaseUtils.InsertHelper ih = new DatabaseUtils.InsertHelper(database, TABLE_NAME);

        // Get the numeric indexes for each of the columns that we're updating
        final int wordColumn = ih.getColumnIndex(WORD);
        final int classColumn = ih.getColumnIndex(CLASS);
        //...
        final int meaningColumn = ih.getColumnIndex(MEANING);
        final long startTime = System.currentTimeMillis();
        try {
            database.execSQL("PRAGMA synchronous=OFF");
            database.setLockingEnabled(false);
            database.beginTransaction();
            Log.e("Time: ", "ELEMENT SIZE" + elements.size());

            for (int j = 0; j < elements.size(); j++) {
                Element element = elements.get(j);
                try {
                    ih.prepareForInsert();
                    ih.bind(wordColumn, element.child(0).text());
                    ih.bind(classColumn, element.child(1).text());
                    ih.bind(meaningColumn, element.ownText());
                    ih.execute();
                } catch (IndexOutOfBoundsException e) {
                    Logger.showInfoLog("Data Not Found");
                }
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
            database.setLockingEnabled(true);
            database.execSQL("PRAGMA synchronous=NORMAL");
            ih.close();

            final long endtime = System.currentTimeMillis();
            Log.e("Time: ", "time to END" + (endtime - startTime) / 1000);

        }
    }

    public void insertData(Elements elements) {
        final String INSERT_QUERY = createInsert(TABLE_NAME, new String[]{WORD, CLASS, MEANING});

        final SQLiteDatabase db = getWritableDatabase();
        final SQLiteStatement statement = db.compileStatement(INSERT_QUERY);
        db.beginTransaction();

        try {
            for (int j = 0; j < elements.size(); j++) {
                Element element = elements.get(j);
                try {
                    statement.clearBindings();
                    statement.bindString(1, element.child(0).text());
                    statement.bindString(2, element.child(1).text());
                    statement.bindString(3, element.ownText());
                    // rest of bindings
                    statement.execute();
                } catch (IndexOutOfBoundsException e) {
                    Logger.showInfoLog("Data Not Found");
                }
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    static public String createInsert(final String tableName, final String[] columnNames) {
        if (tableName == null || columnNames == null || columnNames.length == 0) {
            throw new IllegalArgumentException();
        }
        final StringBuilder s = new StringBuilder();
        s.append("INSERT INTO ").append(tableName).append(" (");
        for (String column : columnNames) {
            s.append(column).append(" ,");
        }
        int length = s.length();
        s.delete(length - 2, length);
        s.append(") VALUES( ");
        for (int i = 0; i < columnNames.length; i++) {
            s.append(" ? ,");
        }
        length = s.length();
        s.delete(length - 2, length);
        s.append(")");
        return s.toString();
    }
}
