package com.scansione.quickdic.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scansione.quickdic.R;
import com.scansione.quickdic.interfaces.SetOnReloadClickListener;
import com.scansione.quickdic.models.RowModel;
import com.scansione.quickdic.tables.RowTable;
import com.scansione.quickdic.viewholders.MainRowHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AJIJUL on 1/8/2017.
 */
public class MainActivityRecyclerAdapter extends RecyclerView.Adapter<MainRowHolder> {
    private List<RowTable> rowModels;
    private SetOnReloadClickListener setOnReloadClickListener;
    private Context context;

    public MainActivityRecyclerAdapter(List<RowTable> rowModels, SetOnReloadClickListener setOnReloadClickListener, Context context) {
        this.rowModels = rowModels;
        this.setOnReloadClickListener = setOnReloadClickListener;
        this.context = context;
    }

    @Override
    public MainRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainRowHolder(LayoutInflater.from(context).inflate(R.layout.row_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(MainRowHolder holder, final int position) {
        RowTable rowModel = rowModels.get(position);
        holder.url.setText(rowModel.getUrls());
        holder.status.setText(rowModel.getStatus());
        holder.rowRecycler_imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOnReloadClickListener.onReload(position);
            }
        });
        if (!rowModel.isLoaded()) {
            holder.status.setTypeface(Typeface.DEFAULT);
            holder.rowRecycler_imv.setImageResource(android.R.drawable.ic_menu_revert);

        } else {
            holder.status.setTypeface(Typeface.DEFAULT_BOLD);
            holder.rowRecycler_imv.setImageResource(android.R.drawable.checkbox_on_background);
        }

    }

    @Override
    public int getItemCount() {
        return rowModels.size();
    }

    public void notifyMyAdapter(List<RowTable> rowModels) {
        this.rowModels = new ArrayList<>(rowModels);
        notifyDataSetChanged();
    }
}
