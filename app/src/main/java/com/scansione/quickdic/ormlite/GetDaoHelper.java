package com.scansione.quickdic.ormlite;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by AJIJUL on 1/8/2017.
 */
public class GetDaoHelper {

    private static OrmLiteDatabaseHelper databaseHelper = null;

    /**
     * You'll need this in your class to get the helper from the manager once per class.
     */
    public static OrmLiteDatabaseHelper getOrmLiteHelper(Context mContext) {
        if (databaseHelper == null) {
            databaseHelper = OrmLiteDatabaseHelper.getHelper(mContext);
        }
        return databaseHelper;
    }

 
    public static void setOrmLiteDatabaseHelper(OrmLiteDatabaseHelper databaseHelperTeacher) {
        GetDaoHelper.databaseHelper = databaseHelperTeacher;
    }

    public static void destroyHelpers(Context mContext) {
    /*
    * You'll need this in your class to release the helper when done.
    */
        if (GetDaoHelper.getOrmLiteHelper(mContext) != null) {
            GetDaoHelper.getOrmLiteHelper(mContext).close();
            OpenHelperManager.releaseHelper();
            GetDaoHelper.setOrmLiteDatabaseHelper(null);
        }



    }
   

}
