package com.scansione.quickdic.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.scansione.quickdic.R;
/**
 * Created by AJIJUL on 1/8/2017.
 */

public class SplashScreenActivity extends AppCompatActivity {

    private Runnable runnable;
    private long SPLASH_TIMEOUT = 3000;
    String TAG = "SplashScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_activity);
        redirectToLandingActivity();


    }

    private void redirectToLandingActivity() {
        runnable = new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                finish();
            }
        };
        new Handler().postDelayed(runnable, SPLASH_TIMEOUT);
    }
}