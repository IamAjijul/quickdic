package com.scansione.quickdic.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.scansione.quickdic.R;
import com.scansione.quickdic.adapters.MainActivityRecyclerAdapter;
import com.scansione.quickdic.gateways.DownloadMyData;
import com.scansione.quickdic.helper.Logger;
import com.scansione.quickdic.helper.SQliteOpenHelper;
import com.scansione.quickdic.interfaces.SetOnDownloading;
import com.scansione.quickdic.interfaces.SetOnReloadClickListener;
import com.scansione.quickdic.models.RowModel;
import com.scansione.quickdic.ormlite.GetDaoHelper;
import com.scansione.quickdic.ormlite.OrmLiteDatabaseHelper;
import com.scansione.quickdic.tables.DicTable;
import com.scansione.quickdic.tables.RowTable;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by AJIJUL on 1/8/2017.
 */
public class MainActivity extends AppCompatActivity implements SetOnDownloading, SetOnReloadClickListener {
    private int positionToDownload = 0;
    private OrmLiteDatabaseHelper ormLiteDatabaseHelper;
    private RecyclerView recyclerView;
    private List<RowTable> rowModelList;
    private MainActivityRecyclerAdapter adapter;
    private Dao<DicTable, Integer> dicTableIntegerDao;
    private Dao<RowTable, Integer> rowTableIntegerDao;
    private ProgressDialog progressDialog = null;
    private TextView tvMainData;
    private LinearLayoutManager linearLayoutManager;
    private final String[] URLS = {"http://unreal3112.16mb.com/wb1913_a.html", "http://unreal3112.16mb.com/wb1913_b.html",
            "http://unreal3112.16mb.com/wb1913_c.html", "http://unreal3112.16mb.com/wb1913_d.html",
            "http://unreal3112.16mb.com/wb1913_e.html", "http://unreal3112.16mb.com/wb1913_f.html",
            "http://unreal3112.16mb.com/wb1913_g.html", "http://unreal3112.16mb.com/wb1913_h.html",
            "http://unreal3112.16mb.com/wb1913_i.html", "http://unreal3112.16mb.com/wb1913_j.html",
            "http://unreal3112.16mb.com/wb1913_k.html", "http://unreal3112.16mb.com/wb1913_l.html",
            "http://unreal3112.16mb.com/wb1913_m.html", "http://unreal3112.16mb.com/wb1913_n.html",
            "http://unreal3112.16mb.com/wb1913_o.html", "http://unreal3112.16mb.com/wb1913_p.html",
            "http://unreal3112.16mb.com/wb1913_q.html", "http://unreal3112.16mb.com/wb1913_r.html",
            "http://unreal3112.16mb.com/wb1913_s.html", "http://unreal3112.16mb.com/wb1913_t.html",
            "http://unreal3112.16mb.com/wb1913_u.html", "http://unreal3112.16mb.com/wb1913_v.html",
            "http://unreal3112.16mb.com/wb1913_w.html", "http://unreal3112.16mb.com/wb1913_x.html",
            "http://unreal3112.16mb.com/wb1913_y.html", "http://unreal3112.16mb.com/wb1913_z.html"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.setLogTag("MainActivity");
        initVariablesAndViews();
    }

    private void initVariablesAndViews() {
        recyclerView = (RecyclerView) findViewById(R.id.mainActivityRecyclerView);
        tvMainData = (TextView) findViewById(R.id.tvMainData);
        ormLiteDatabaseHelper = GetDaoHelper.getOrmLiteHelper(this);
        dicTableIntegerDao = ormLiteDatabaseHelper.getDicTableDao();
        rowTableIntegerDao = ormLiteDatabaseHelper.getRowTableDao();
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        rowModelList = new ArrayList<>();
        try {
            rowModelList = rowTableIntegerDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        adapter = new MainActivityRecyclerAdapter(rowModelList, this, this);
        recyclerView.setAdapter(adapter);
        int arraySize = rowModelList.size();
        if (arraySize == 0)
            new DownloadMyData(this, this, 0).execute(URLS[positionToDownload]);
        else if (arraySize < URLS.length) {
            new DownloadMyData(this, this, arraySize - 1).execute(URLS[arraySize - 1]);
        }
    }

    @Override
    public void onComplete(int i, Document document) {
        final Elements elements = document.select("p");
        Logger.showInfoLog("OnComplete" + elements.size());
        new SaveToDB(elements, i, this).execute();
    }

    @Override
    public void onError(int i) {
        Logger.showInfoLog("OnError" + i);
        tvMainData.setText(URLS[i] + "\n\n Not Responding");

        try {
            rowModelList.set(i, new RowTable(URLS[i], "Not Responding", false));
        } catch (IndexOutOfBoundsException e) {
            rowModelList.add(i, new RowTable(URLS[i], "Not Responding", false));

        }
        adapter.notifyMyAdapter(rowModelList);
        linearLayoutManager.scrollToPosition(i);
        try {
            rowTableIntegerDao.createOrUpdate(new RowTable(URLS[i], "Not Responding", false));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (i + 1 < URLS.length && rowModelList.size() != URLS.length) {
            new DownloadMyData(this, this, i + 1).execute(URLS[i + 1]);
        }


    }


    @Override
    public void onReload(int position) {
        RowTable rowTable = rowModelList.get(position);
        if (rowTable.isLoaded()) {
            tvMainData.setText(rowTable.getUrls() + "\n\n EXEC TIME :" + rowTable.getStatus());
        } else
            new DownloadMyData(this, this, position).execute(URLS[position]);

    }

    class SaveToDB extends AsyncTask<Void, String, Integer> {
        private Elements elements;
        private int position;
        private Context context;
        private ProgressDialog progressDialog;
        private long startTime, elapsedTime;

        public SaveToDB(Elements elements, int position, Context context) {
            this.context = context;
            this.elements = elements;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startTime = System.currentTimeMillis();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Saving...");
            }
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            SQliteOpenHelper sqLiteOpenHelper = new SQliteOpenHelper(MainActivity.this);
            sqLiteOpenHelper.insertData(elements);
            /*for (int j = 0; j < elements.size(); j++) {
                Element element = elements.get(j);
                try {
                    try {
                        dicTableIntegerDao.createIfNotExists(new DicTable(element.child(0).text(),
                                element.child(1).text(), element.ownText()));
                    } catch (IndexOutOfBoundsException e) {
                        Logger.showInfoLog("Data Not Found");
                    }
                    if (j % 10 == 0) {
                        long elapsedTime = System.currentTimeMillis() - startTime;
                        String str = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(elapsedTime), (int) elapsedTime / 1000
                                , (int) elapsedTime % 1000);
                        publishProgress(str);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            progressDialog.dismiss();
            elapsedTime = System.currentTimeMillis() - startTime;
            String str = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(elapsedTime), (int) elapsedTime / 1000
                    , (int) elapsedTime % 1000);
            tvMainData.setText(URLS[position] + "\n\n EXEC TIME :" + str);
            try {
                rowModelList.set(position, new RowTable(URLS[position], str, true));
            } catch (IndexOutOfBoundsException e) {
                rowModelList.add(position, new RowTable(URLS[position], str, true));

            }
            adapter.notifyMyAdapter(rowModelList);
            linearLayoutManager.scrollToPosition(position);
            try {
                rowTableIntegerDao.createOrUpdate(new RowTable(URLS[position], str, true));
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (position + 1 < URLS.length && rowModelList.size() != URLS.length) {
                new DownloadMyData(MainActivity.this, MainActivity.this, position + 1).execute(URLS[position + 1]);
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progressDialog.setMessage("EXEC TIME : " + values[0]);
        }
    }
}
