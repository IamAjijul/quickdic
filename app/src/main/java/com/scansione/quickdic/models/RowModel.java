package com.scansione.quickdic.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AJIJUL on 1/8/2017.
 */

public class RowModel implements Parcelable {
    String url, status;

    protected RowModel(Parcel in) {
        url = in.readString();
        status = in.readString();
    }

    public RowModel(String url, String status) {
        this.url = url;
        this.status = status;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RowModel> CREATOR = new Creator<RowModel>() {
        @Override
        public RowModel createFromParcel(Parcel in) {
            return new RowModel(in);
        }

        @Override
        public RowModel[] newArray(int size) {
            return new RowModel[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
