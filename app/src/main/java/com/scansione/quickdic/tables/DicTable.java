package com.scansione.quickdic.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by AJIJUL on 1/8/2017.
 */
@DatabaseTable(tableName = "DictionaryTable")
public class DicTable {
    @DatabaseField(columnName = "Word",canBeNull = false)
    String word;
    @DatabaseField(columnName = "Class",canBeNull = false)
    String wordClass;
    @DatabaseField(columnName = "Meaning",canBeNull = false)
    String wordMeaning;
    @DatabaseField(columnName = "id",id = true)
    String id;

    public DicTable() {
    }

    public DicTable(String word, String wordClass, String wordMeaning) {
        this.word = word;
        this.wordClass = wordClass;
        this.wordMeaning = wordMeaning;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWordClass() {
        return wordClass;
    }

    public void setWordClass(String wordClass) {
        this.wordClass = wordClass;
    }

    public String getWordMeaning() {
        return wordMeaning;
    }

    public void setWordMeaning(String wordMeaning) {
        this.wordMeaning = wordMeaning;
    }
}
