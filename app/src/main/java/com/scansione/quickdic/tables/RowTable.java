package com.scansione.quickdic.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by AJIJUL on 1/8/2017.
 */
@DatabaseTable(tableName = "RowTable")
public class RowTable {
    @DatabaseField(columnName = "URLS", canBeNull = false, id = true)
    String urls;
    @DatabaseField(columnName = "Status", canBeNull = false)
    String status;
    @DatabaseField(columnName = "IsLoaded", canBeNull = false)
    boolean isLoaded;

    public RowTable() {
    }

    public RowTable(String urls, String status, boolean isLoaded) {
        this.urls = urls;
        this.status = status;
        this.isLoaded = isLoaded;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setLoaded(boolean loaded) {
        isLoaded = loaded;
    }
}
