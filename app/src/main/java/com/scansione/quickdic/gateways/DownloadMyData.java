package com.scansione.quickdic.gateways;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.scansione.quickdic.helper.Logger;
import com.scansione.quickdic.interfaces.SetOnDownloading;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import static android.util.Log.e;

/**
 * Created by AJIJUL on 1/8/2017.
 */

public class DownloadMyData extends AsyncTask<String, Integer, Boolean> {
    private SetOnDownloading setOnDownloadingListener;
    private int positionToDwld;
    private Document document = null;
    private ProgressDialog progressDialog = null;
    private Context context;

    public DownloadMyData(Context context, SetOnDownloading setOnDownloadingListener, int positionToDwld) {
        this.context = context;
        this.setOnDownloadingListener = setOnDownloadingListener;
        this.positionToDwld = positionToDwld;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Downloading...");
        }
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        try {

            URL url = new URL(strings[0]);
            //create the new connection
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            //set up some things on the connection
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();

            Logger.showInfoLog("URL :-:" + strings[0] + ", Respond Code :-:" + httpURLConnection.getResponseCode());
            if (httpURLConnection.getResponseCode() >= 300) {
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String inputLine = "";
                while ((inputLine = br.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                document = Jsoup.parse(stringBuilder.toString());
                return true;
            } else
                throw new IOException("Invalid Response code :" + httpURLConnection.getResponseCode());
        } catch (IOException e1) {
            //  e1.printStackTrace();

            try {
                Logger.showInfoLog(positionToDwld + " : URL :-:" + strings[0] +
                        ",Exception On HttpURLConnection, retrying...");
                document = Jsoup.connect(strings[0]).get();
                return true;

            } catch (IOException e) {
                // e.printStackTrace();
                return false;
            }

        }

    }

    @Override
    protected void onPostExecute(Boolean b) {
        super.onPostExecute(b);
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        if (b) {
            setOnDownloadingListener.onComplete(positionToDwld, document);
        } else {
            setOnDownloadingListener.onError(positionToDwld);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }
}
