package com.scansione.quickdic.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansione.quickdic.R;

/**
 * Created by AJIJUL on 1/8/2017.
 */

public class MainRowHolder extends RecyclerView.ViewHolder {
    public ImageView rowRecycler_imv;
    public TextView url, status;


    public MainRowHolder(View itemView) {
        super(itemView);
        rowRecycler_imv = (ImageView) itemView.findViewById(R.id.rowRecycler_imvReload);
        url = (TextView) itemView.findViewById(R.id.mainActivity_tvUrl);
        status = (TextView) itemView.findViewById(R.id.mainActivity_tvStatus);
    }
}
