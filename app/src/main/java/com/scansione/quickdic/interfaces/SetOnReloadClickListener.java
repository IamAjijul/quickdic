package com.scansione.quickdic.interfaces;

/**
 * Created by AJIJUL on 1/8/2017.
 */

public interface SetOnReloadClickListener {
    void onReload(int position);
}
