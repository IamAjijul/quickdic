package com.scansione.quickdic.interfaces;

import org.jsoup.nodes.Document;

/**
 * Created by AJIJUL on 1/8/2017.
 */

public interface SetOnDownloading {
    void onComplete(int i, Document document);

    void onError(int i);
}
